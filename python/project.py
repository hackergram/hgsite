import ftplib
import pygsheets
import pandas
from telegram.ext import Updater, CommandHandler
import requests
import re


def update_name(key,text,attribute):
    page=pygsheets.authorize(key)
    members=page.open_by_key("1Alk0lPhlgkBCs3vQClejZTiGLVsHDVUkUfyqn-YnKXM")
    lis_members=members.worksheet()
    lis=lis_members.get_row(row=1)
    if attribute  in lis:
        try:
            lis_members.update_cell((len(lis_members.get_col(lis.index(attribute)+1,include_empty=False))+1,lis.index(attribute)+1),text)
        except Exception as e:
            print(e)
            
    else:
        raise Exception("Please insert a row header as "+attribute+" and try again")
